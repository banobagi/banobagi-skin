$(document).ready(function(){

	if ($(window).scrollTop()<50) {
		$('.header').stop().addClass('type-main');
	}
	else {
		$('.header').stop().removeClass('type-main');
	}
	$(window).scroll(function() {
		var mCheck = $('.header').attr('class');
		var mSplit = mCheck.split(' ')
		var isPopup = $('body').attr('class')
		if (($(this).scrollTop()<50) && (mSplit[1] != 'js-open-m') && (isPopup != 'no-scroll')) {
		    $('.header').stop().addClass('type-main');
		}
		else {
		    $('.header').stop().removeClass('type-main');
		}
	});
	$(window).on("load resize", function () {
		var winH = $(window).height();
		$('.intro, .detail').css({height: winH});
		$('body').attr('data-mobile',
			(function(){
				var r = ($(window).width() <= 1024) ? true : false;
				return r;
			}())
		);

		$('body').attr('only-mobile',
			(function(){
				var r = ($(window).width() <= 680) ? true : false;
				return r;
			}())
		);
		//라인 변수값
        icw = $('.intro-inner').width();
        ich = $('.intro-inner').height();
        snbW = 99;
        snsH = $('.intro-sns').height();
        leftTop = 40;
        borderRight = 48;
        borderBottom = (icw/2)-18;

		function resize_fw(){
			var winW = $(window).width();
			perW = winW/4;
			$(".detail__item").removeClass('is-active').css('height','100vh').width(perW)
		}

		function resize_fh(){
			var winH = $(window).height();
			perH = winH/4;
			$(".detail__item").removeClass('is-active').css('width','100vw').height(perH)
		}

		if ($('body').attr('data-mobile') == 'false'){
			//라인
			snbW = 140; //윗쪽선 조정
			borderRight = 24; //오른쪽선 조정

			//detail
			resize_fw()
			$(".detail__item").off('click').on('mouseenter',function(){
				$(".detail__item").removeClass("is-active")
				$(this).addClass("is-active")
				.stop().animate({"width":(perW+224)+"px"},300)
				.find(".darker").stop().fadeTo(300,0)
				$(".detail__item:not(.is-active)")
				.stop().animate({"width":(perW-(224/4))+"px"},300)
				.find(".darker").stop().fadeTo(300,1)
			})
			$('.detail').on('mouseleave',function(){
				$(".detail__item").removeClass("is-active").stop().animate({"width":perW},300)
			});

		}else{
			//라인
			snbW = 100; //윗쪽선 조정
			borderRight = 24; //오른쪽선 조정
			//detail
			resize_fh()
			$(".detail__item").off('mouseenter').on('click',function(){
				$(".detail__item").removeClass("is-active")
				$(this).addClass("is-active")
				.stop().animate({"height":(perH+140)+"px"},300);
				$(".detail__item:not(.is-active)")
				.stop().animate({"height":(perH-(140/4))+"px"},300);
			});
			$('.detail').off('mouseleave');
			if($('body').attr('only-mobile') == 'true'){
				snbW = 28; //윗쪽선 조정
				borderRight = 20; //오른쪽선 조정
				resize_fh()
				$(".detail__item").off('mouseenter').on('click',function(){
					$(".detail__item").removeClass("is-active")
					$(this).addClass("is-active")
					.stop().animate({"height":(perH+120)+"px"},300);
					$(".detail__item:not(.is-active)")
					.stop().animate({"height":(perH-(120/4))+"px"},300);
				});
				$('.detail').off('mouseleave');
			}
		}
		//영상제어
		var f;
		var url;
		$(document).ready(function($){
		    f = $(".intro-video__iframe");
		    if(!f || !f.attr("src") ) return;
		    url = f.attr("src").split("?")[0];
		});
		//인트로 내리기
		$('.js-intro-close').on('click',function(e){
			e.preventDefault();
			var secondPos = $('.detail');
			var fullPos = secondPos.offset().top;
			$("html,body").stop().animate({scrollTop:fullPos});
		});
		//마우스휠
		$('.intro').on("mousewheel DOMMouseScroll",function(e){
		    var delta  = e.originalEvent.wheelDelta || e.originalEvent.detail*-1 || e.originalEvent.deltaY*-1;
		    var secondPos = $('.detail');
		    var fullPos = secondPos.offset().top;
		    if(delta >= 0) {
		        $("html,body").stop().animate({scrollTop:0});
		    } else if (delta < 0) {
		        $("html,body").stop().animate({scrollTop:fullPos});
		        var data = {
		            method: "pause",
		            value: 1
		        };
		        f[0].contentWindow.postMessage(JSON.stringify(data), url);
		        return false;
		    }
		});
		$('.detail').on("mousewheel DOMMouseScroll",function(e){
		    var delta  = e.originalEvent.wheelDelta || e.originalEvent.detail*-1 || e.originalEvent.deltaY*-1;
		    var secondPos = $('.intro');
		    var fullPos = secondPos.offset().top;
		    if(delta >= 0 ) {
		        $("html,body").stop().animate({scrollTop:0});
		        if(fullPos == 0){
		            var data = {
		                method: "play",
		                value: 1
		            };
		            f[0].contentWindow.postMessage(JSON.stringify(data), url);
		        }
		        return false;
		    }
		});
	});//load,resize
	$(window).on("resize", function () {
		//라인 그리기(애니메이션X)
        $("#borderTopBar").css({width: icw - snbW});
        $("#borderLeftBar01").css({height: leftTop});
        $("#borderLeftBar02").css({'bottom':'0','height':ich - snsH - leftTop - 20});
        $("#borderBottomBar01").css({'bottom':'0','width': borderBottom});
        $("#borderBottomBar02").css({'bottom':'0','right':'0','width':borderBottom});
        $("#borderRightBar").css({'bottom':'0','right':'0','height':ich - borderRight});
    });//resize
    $(window).on('load',function(){
        //라인 그리기(애니메이션)
        $("#borderTopBar").animate({width: icw - snbW},1000);
        $("#borderLeftBar01").animate({height: leftTop},500);
        $("#borderLeftBar02").css({'bottom':'0'}).animate({height: ich - snsH - leftTop - 20},1000);
        $("#borderBottomBar01").css({'bottom':'0'}).animate({width: borderBottom},1000);
        $("#borderBottomBar02").css({'bottom':'0','right':'0'}).animate({width: borderBottom},1000);
        $("#borderRightBar").css({'bottom':'0','right':'0'}).animate({height:ich - borderRight},1000);
        //텍스트 모션
        $('.js-main-txt1').animate({'opacity':'1'},2000,function(){
            $('.js-main-txt2').animate({'opacity':'1'},2000,function(){
                var typed = new Typed('#typed', {
                    stringsElement: '#typed-strings',
                    typeSpeed: 100,
                    backSpeed: 0,
                    startDelay: 0,
                    loop: false,
                    loopCount: Infinity,
                    onStringTyped:function(){
                        $('.js-main-txt3').animate({'opacity':'1'},2000,function(){
                            $('.typed-cursor').removeClass('is-focus').delay(500).animate({'opacity':'0'},1400);
                        });
                    }
                });
                $('.typed-cursor, .intro-inner__tit').css('opacity','1');
                $('.typed-cursor').addClass('is-focus');
            });
        });
    });//load
});

//포커스픽
$('.focus__list').slick({
    autoplay: true, //자동슬라이드
    prevArrow: '.js-focus-prev',
    nextArrow: '.js-focus-next',
    slidesToShow:3,
    slidesToScroll: 3,
    centerPadding:'10px',
    infinite: true,

    dots:true,
    dotsClass:'focus__dot slide-dot',
    responsive: [
        {
            breakpoint: 993,
            arrows:false,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        },
        {
            breakpoint: 681,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }
    ]
});
//메인 검색
$('#main_keyword').focus(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').addClass('is-focus');
})
.blur(function() {
	$(this).parents('.search-keyword').find('.search-keyword__btn').removeClass('is-focus');
});
//디테일 슬라이드
$('.js-detail-slide').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 4000,
	dotsClass:'detail-slide__dot slide-dot'
});

//오시는길 문자보내기팝업
$('.js-sms-btn').on('click',function(e){
	e.preventDefault();
	$('.main-form').addClass('is-active');
});
$('.js-sms-close').on('click',function(e){
	e.preventDefault();
	$('.main-form').removeClass('is-active');
});

//포토갤러리 팝업
$('.js-photo-open').on('click',function(e){
	e.preventDefault();
	$('.photo-slide__view').removeClass('is-active');
	$('.main-photo, .photo-slide__popup').addClass('is-active');
});
$('.js-photo-close').on('click',function(e){
	e.preventDefault();
	$('.photo-slide__view').addClass('is-active');
	$('.main-photo, .photo-slide__popup').removeClass('is-active');
});

//포토갤러리 자동슬라이드
$('.photo-slide__slide').owlCarousel({
	loop:true,
	dots:false,
	items:1,
	animateIn:true,
	mouseDrag:false,
	touchDrag:true,
	pullDrag:true
});

var owl = $('.owl-carousel');
// Go to the next item
$('.photo-slide__btn.type-next').click(function() {
    owl.trigger('next.owl.carousel');
})
// Go to the previous item
$('.photo-slide__btn.type-prev').click(function() {
    owl.trigger('prev.owl.carousel');
})
