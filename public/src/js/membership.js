$(document).ready(function($) {
	$(window).on('load',function(){
		navH = $('.header').height()+5;
		$(".etc__box-link").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top-navH}, 500);
	    });
	});
});





// check box
$('#frmJoinStep1 #join_chk_agree_all').click(function(){
  $('#frmJoinStep1 input:checkbox').not(this).prop('checked', this.checked).keyup();
});

$('input:checkbox').not('#frmJoinStep1 #join_chk_agree_all').click(function() {
  if($('#frmJoinStep1 .checkbox01.type-another:checked').length == 3) {
    $('#frmJoinStep1 #join_chk_agree_all').prop('checked', true);
  }else {
    $('#frmJoinStep1 #join_chk_agree_all').prop('checked', false);
  }
});

//회원가입 STEP1
$("#frmJoinStep1").validate({
	errorPlacement: function(error, element) {
		if (element.attr("name") == "join_chk_agree1") {
			error.insertAfter("#join_chk_agree1 + label");
		}else if(element.attr("name") == "join_chk_agree2"){
			error.insertAfter("#join_chk_agree2 + label");
		}else if(element.attr("name") == "join_chk_agree3"){
			error.insertAfter("#join_chk_agree3 + label");
		}else {
			error.insertAfter(element);
		}
	},
	messages: {
		join_chk_agree1: "* 회원가입약관에 동의해주세요.",
		join_chk_agree2: "* 개인정보취급방침에 동의해주세요.",
		join_chk_agree3: "* 개인정보 수집 및 이용에 동의해주세요."
	},
	submitHandler: function(form) {
		 form.submit();
	}
});


$.validator.addMethod("passwordCk",  function( value, element ) {
	return this.optional(element) ||  /^.*(?=.*\d)(?=.*[a-zA-Z]).*$/.test(value);
});

//회원가입 STEP2
$("#frmJoinStep2").validate({
    rules: {
      userId: {
        required: true,
        minlength: 5,
        maxlength: 12
      },
      userPw: {
        required: true,
		passwordCk : true,
        minlength: 10,
        maxlength: 15
      },
      userPwConfirm: {
        required: true,
        minlength: 10,
        maxlength: 15,
		equalTo: '#userPw'
      },
      userName: {
        required: true,
        minlength: 2,
        maxlength: 10
      }
    },
    errorPlacement: function(error, element) {
        error.insertAfter(element);
    },
    messages: {
      userId: {
        required: '* 아이디를 입력해주세요.',
        minlength: '* 5~12자리로 입력해 주세요.',
        maxlength: '* 5~12자리로 입력해 주세요.'
      },
      userPw: {
        required: '* 비밀번호를 입력해주세요.',
        passwordCk: '* 영문+숫자 10~15자리로 입력해 주세요.',
        minlength: '* 영문+숫자 10~15자리로 입력해 주세요.',
        maxlength: '* 영문+숫자 10~15자리로 입력해 주세요.'
      },
	  userPwConfirm: {
        required: '* 비밀번호를 한번 더 입력해주세요.',
		equalTo: '비밀번호가 일치하지 않습니다.',
        minlength: '* 영문+숫자 10~15자리로 입력해 주세요.',
        maxlength: '* 영문+숫자 10~15자리로 입력해 주세요.'
	  },
      userName: {
        required: '* 이름을 입력해주세요.',
        minlength: '* 2~10자리로 입력해 주세요.',
        maxlength: '* 2~10자리로 입력해 주세요.'
      },
      userPhone: '* 휴대번화를 입력해주세요.',
      userConfirmNumber: '* 인증번호를 입력해주세요.',
      userEmail: {
        required: '* 이메일을 입력해주세요.',
        email: '올바른 형식의 이메일 주소를 입력해 주십시오.'
      }
    },
    submitHandler: function(form) {
		if($('#chkUserIdVal').val() == '') {
			alert('중복확인을 통해 아이디 중복확인을 해주세요!!');
			return false;
		}
		if($('#chkUserIdVal').val() !=  $.trim($('#userId').val())) {
			alert('아이디 중복확인을 해주세요!!');
			return false;
		}

		/*if($('#userPhone').val() !=  $.trim($('#hpConfirm').val())) {
			alert('휴대전화 인증을 통해 인증 해주세요!!');
			return false;
		}*/

		 //휴대폰인증확인
		/*if ($('#chkHpConfirm').val() != "Y") {
			alert("휴대전화 인증을 확인하세요.");
			return;
		}*/


		form.submit();
    }
  });


// 아이디 중복확인
$('#btnUserIdChk').click(function(){
	var userId = $.trim($('#userId').val());
	if(userId == ''){
		alert('아이디를 등록하세요!!');
		$('#userId').focus();
		return false;
	}

	if( userId.length < 5 || userId.length > 12 )   {
		alert('아이디는 영문+숫자의 조합으로 5 - 12자리입니다.');
		$('#userId').focus();
		return false;
	}

	var pattern1 = /(^[a-zA-Z])/;
	var pattern2 = /([^a-zA-Z0-9])/;

	if(!pattern1.test(userId)){
		alert('아이디의 첫글자는 영문이어야 합니다.');
		return false;
	}

	if(pattern2.test(userId)){
		alert('아이디는 영문, 숫자만 사용할 수 있습니다.');
		return false;
	}

	$.post('/membership/membershipJoinStep2Proc.php', {userId:userId,modeType:'USERIDCHK'}, function(data){
		if(data == 1)
		{
			$('#chkUserIdVal').val(userId);
			alert('등록 가능한 아이디입니다');
		}else{
			$('#chkUserIdVal').val('');
			$('#userId').val('')
			alert('기등록된 아이디입니다. 다른 아이디로 등록해주세요.');
			$('#userId').focus();
		}
		return false;
	});
});

//휴대폰인증전송
$('#btnHpConfirmSend').click(function(){
	var hp_com
	hp_com = 2;

	var hp_num;
	hp_num = $('input[name=userPhone]').val();

	if(hp_num == "")
	{
		alert('핸드폰번호를 등록하세요!!');
		$('#userPhone').focus();
		return false;
	}

	$.post('/membership/scheckplus_step1.php', { hp_num: hp_num, hpcomp: hp_com }, function (data) {
		var result = data.split('|');
		if (result[0] == '0000') {
			alert('인증번호가 문제메세지로 발송되었습니다.');
			$('input[name=requestSQ]').attr('value', result[1]); //요청
			$('input[name=responseSQ]').attr('value', result[2]); //응답
		} else {
			alert('번호를 확인해주세요.');
		}
	});

});

//휴대폰인증확인
$('#btnHpConfirmOK').click(function(){
	var hp_req, hp_res, authNum
	requestSQ = $('input[name=requestSQ]').val();
	responseSQ = $('input[name=responseSQ]').val();
	authNum = $('input[name=userConfirmNumber]').val();

	$.post('/membership/scheckplus_step2.php', { requestSQ: requestSQ, responseSQ: responseSQ, authNum: authNum }, function (data) {
		//alert(data);
		var result = data.split('|');
		if (result[0] == '0000') {
			alert('인증확인되셨습니다.');
			$('#chkHpConfirm').val('Y');
			$('#hpConfirm').val($('input[name=userPhone]').val());
		} else {
			alert('인증번호를 확인해주세요.');
			$('#userConfirmNumber').val('');
			$('#userConfirmNumber').focus();
		}

	});
});
