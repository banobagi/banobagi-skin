$(document).ready(function(){
	//input 닫기
	var $ipt = $('.result-search__input'),
	    $clearIpt = $('.result-search__del');

	$ipt.keyup(function(){
		$(this).next().toggle(Boolean($(this).val()));
	});
	$clearIpt.toggle(Boolean($ipt.val()));
	$clearIpt.click(function(e){
		e.preventDefault();
		$(this).prev().val('').focus();
		$(this).hide();
	});
	$('.js-more-btn').on('click',function(e){
		e.preventDefault();
		$(this).hide().parents('.result__dd').find('.is-hide').removeClass('is-hide');
	});

	// 검색 결과 탭 스크롤
	$('.result-tab__link').click(function(e) {
		e.preventDefault();
		if(!$(this).hasClass('type-all')) {
			var liFind = $(this).parent().index()-1;
			var headerH = $('.header').height();
			$('html, body').animate({
				scrollTop: $('.result__box').eq(liFind).offset().top-headerH
			}, 1000);
			return false;
		} else {
			return false
		}
	});

});
