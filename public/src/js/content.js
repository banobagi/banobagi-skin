$(document).ready(function(){
	$(window).on("load resize", function () {
		headerH = $('.header').height();
		$('.js-quick-link').on('click',function(e){
			e.preventDefault();
			var dataName = $(this).attr('data-tab');
			var dataSet = $('.'+dataName).offset().top - headerH - 40;
			$("html, body").animate({
				scrollTop:dataSet
			},500);
		});
		//bfaf
		if ($('body').attr('only-mobile') == 'false' && $('body').attr('data-mobile') == 'true' && $('.js-bfaf-open').hasClass('is-active')){
			setTimeout(function() {
				$('.js-bfaf-open, .bfaf').removeClass('is-active')
				notiHide();
			},2000);
		}
	});
	//bfaf
	function notiHide(){
		$('.side-noti').addClass('is-active')
		setTimeout(function() {
			$('.side-noti').removeClass('is-active')
		},2000);
	}
	$('.js-bfaf-open').on('click',function(e){
		e.preventDefault();
		$(this).toggleClass('is-active');
		$('.bfaf').toggleClass('is-active');
		if($(this).hasClass('is-active')){

		}else{
			notiHide();
		}
	});
	$('.tab-v1__link').on('click',function(e){
		e.preventDefault();
		var tabClass = $(this).attr('href');
		$('.tab-v1__item, .tab-content').removeClass('is-active');
		$(this).parents('.tab-v1__item').addClass('is-active');
		$("."+tabClass).addClass('is-active');
	});
});
