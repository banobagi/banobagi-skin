$(document).ready(function(){
	$('.js-link-hover').on('mouseenter',function(){
		$(this).parent().addClass('is-active');
	}).on('mouseleave',function(){
		$(this).parent().removeClass('is-active');
	});
	//갤러리 컨텐츠 열기/닫기
	var galleryCont = $('.gallery__content');
	$('.js-gallery-link').on('click',function(e){
		e.preventDefault();
		if(galleryCont.hasClass('is-active')){
			if($(this).parents('.gallery__item').hasClass('is-active')){
				galleryCont.slideUp(500).removeClass('is-active');
				$(this).parents('.gallery__item').removeClass('is-active');
			}else{
				$('.gallery__item').removeClass('is-active');
				$(this).parents('.gallery__item').addClass('is-active');
			}
		}else{
			$(this).parents('.gallery__item').addClass('is-active');
			galleryCont.slideDown(500).addClass('is-active');
		}
	});

	//갤러리 컨텐츠 닫기
	$('.js-gallery-close').on('click',function(e){
		e.preventDefault();
		$('.gallery__item').removeClass('is-active');
		galleryCont.slideUp(500).removeClass('is-active');
	});
	// 맨위로
    $('.js-scroll-up').on('click', function(e) {
		e.preventDefault();
		$('html, body').stop().animate({
			scrollTop: 0
		}, 500);
    });

	// 아래로
	$('.js-scroll-down').on('click', function(e) {
		e.preventDefault();
		$('html, body').stop().animate({
			scrollTop: $(document).height()
		}, 500);
	});
})
$(document).ready(function(){
	$(window).on("load resize", function () {
		var scrollBtn = $('.scroll-btn').height();
		if(scrollBtn != null){
			var galleryH = $('.gallery__wrap').offset().top - scrollBtn -10;
			$('.scroll-btn').css('top',galleryH);
		}
	});
});
