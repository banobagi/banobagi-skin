
// Google API
function initMap() {
  var myLatLng = {lat: 37.503084, lng: 127.035493};
  var centerLatLng = {lat : 37.502660, lng: 127.035795};
  var zoomSize = 17;
  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {	//MOBILE
	  zoomSize = 16;
  }

  // Create a map object and specify the DOM element for display.
  var map = new google.maps.Map(document.getElementById('map'), {
    center: myLatLng,
    scrollwheel: false,
    zoom: 17
  });

  var iconBase = 'http://re.skinbanobagi.com/images/common/';
  // Create a marker and set its position.
  var marker = new google.maps.Marker({
    map: map,
    position: myLatLng,
    icon: iconBase + 'banobagi_marker.png',
	size: new google.maps.Size(85, 52),
    title: 'Banobagi'
  });
}

$(window).load(function() {
	initMap();
});
function open_win()
{
  window.open('../introduce/introduce_print.html','오시는길', 'width=760, height=960, left=100px, top=100px, toolbar=no, location=no, directories=no, status=no, menubar=no, resizable=no, scrollbars=no, copyhistory=no');
}
